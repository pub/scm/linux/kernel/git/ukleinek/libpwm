/* SPDX-License-Identifier: LGPL-2.1-only */
/* SPDX-FileCopyrightText: 2023 Uwe Kleine-König <u.kleine-koenig@pengutronix.de> */

#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <pwm.h>

#include "pwm-internal.h"

struct pwm_sysfs {
	struct pwm pwm;
	int dirfd;

	/* .wf tracks the wf assuming the PWM is enabled. */
	struct pwm_waveform wf;
	bool enabled;
	bool cache_valid;
};

struct pwm_chip_sysfs {
	struct pwm_chip chip;
	int dirfd;
	struct pwm_sysfs *pwms[];
};

static void pwm_chip_sysfs_close(struct pwm_chip *chip)
{
	unsigned int i;
	struct pwm_chip_sysfs *chip_sysfs = container_of(chip, struct pwm_chip_sysfs, chip);

	for (i = 0; i < chip->npwm; ++i) {
		if (chip_sysfs->pwms[i]) {
			close(chip_sysfs->pwms[i]->dirfd);
			free(chip_sysfs->pwms[i]);
		}
	}
	free(chip_sysfs);
}

static struct pwm *pwm_chip_sysfs_get_pwm(struct pwm_chip *chip,
					  unsigned int offset)
{
	struct pwm_chip_sysfs *chip_sysfs = container_of(chip, struct pwm_chip_sysfs, chip);
	struct pwm_sysfs *pwm_sysfs;
	struct pwm *pwm;
#if UINT_MAX <= 4294967295U
	/* force a compiler error if buffer gets too small */
	char pwmX[14];
#endif
	int fd;

	if (chip_sysfs->pwms[offset])
		return &chip_sysfs->pwms[offset]->pwm;

	pwm_sysfs = calloc(1, sizeof(*pwm_sysfs));
	if (!pwm_sysfs)
		return NULL;
	pwm = &pwm_sysfs->pwm;

	pwm->chip = chip;

	sprintf(pwmX, "pwm%u", offset);

	pwm_sysfs->dirfd = openat(chip_sysfs->dirfd, pwmX,
				  O_PATH | O_CLOEXEC);
	if (pwm_sysfs->dirfd < 0 && errno == ENOENT) {
		int ret;

		fd = openat(chip_sysfs->dirfd, "export", O_WRONLY | O_CLOEXEC);
		if (fd < 0) {
			free(pwm_sysfs);
			return NULL;
		}

		ret = dprintf(fd, "%d\n", offset);
		if (ret < 0) {
			close(fd);
			free(pwm_sysfs);
			return NULL;
		}

		ret = close(fd);
		if (ret < 0) {
			free(pwm_sysfs);
			return NULL;
		}

		pwm_sysfs->dirfd = openat(chip_sysfs->dirfd, pwmX,
					  O_PATH | O_CLOEXEC);
	}

	if (pwm_sysfs->dirfd < 0) {
		free(pwm_sysfs);
		return NULL;
	}

	chip_sysfs->pwms[offset] = pwm_sysfs;

	pwm_sysfs->cache_valid = false;

	return pwm;
}

static int pwm_chip_sysfs_write_prop(const struct pwm_sysfs *pwm_sysfs,
					 char *propname,
					 const char *restrict format, ...)
{
	int fd;
	va_list ap;
	int ret;

	fd = openat(pwm_sysfs->dirfd, propname, O_WRONLY | O_CLOEXEC);
	if (fd < 0)
		return -1;

	va_start(ap, format);

	ret = vdprintf(fd, format, ap);

	va_end(ap);

	if (ret < 0) {
		int saved_errno = errno;

		close(fd);

		errno = saved_errno;

		return ret;
	}
	return close(fd);
}

static int pwm_chip_sysfs_set_waveform(struct pwm *pwm,
				       const struct pwm_waveform *wf)
{
	struct pwm_sysfs *pwm_sysfs = container_of(pwm, struct pwm_sysfs, pwm);
	int ret;

	/* period_length_ns = 0 is interpreted as disabled */
	if (wf->period_length_ns == 0) {
		if (!pwm_sysfs->enabled) {
			ret = pwm_chip_sysfs_write_prop(pwm_sysfs, "enable", "0\n");
			if (ret)
				return ret;
			pwm_sysfs->enabled = false;
		}

		return 0;
	}

	if (!pwm_sysfs->cache_valid ||
	    (wf->duty_offset_ns < wf->period_length_ns - wf->duty_length_ns) !=
	     (pwm_sysfs->wf.duty_offset_ns < pwm_sysfs->wf.period_length_ns - pwm_sysfs->wf.duty_length_ns)) {
		if (wf->duty_offset_ns < wf->period_length_ns - wf->duty_length_ns) {
			ret = pwm_chip_sysfs_write_prop(pwm_sysfs, "polarity", "normal\n");
			if (ret)
				return ret;

			pwm_sysfs->wf.duty_offset_ns = 0;
		} else {
			ret = pwm_chip_sysfs_write_prop(pwm_sysfs, "polarity", "inversed\n");
			if (ret)
				return ret;

			pwm_sysfs->wf.duty_offset_ns = wf->period_length_ns - wf->duty_length_ns;
		}
	}

	/*
	 * Ensure that we never hit duty_length_ns > period_length_ns. As updating
	 * period_length_ns and duty_length_ns cannot be done in a single step write
	 * period_length_ns first if period_length_ns increases and write duty_length_ns first
	 * if period_length_ns decreases.
	 */
	if (!pwm_sysfs->cache_valid ||
	    pwm_sysfs->wf.period_length_ns <= wf->period_length_ns) {
		if (!pwm_sysfs->cache_valid ||
		    pwm_sysfs->wf.period_length_ns != wf->period_length_ns) {
			ret = pwm_chip_sysfs_write_prop(pwm_sysfs, "period",
							"%" PRIu64 "\n", wf->period_length_ns);
			if (ret)
				return ret;
			pwm_sysfs->wf.period_length_ns = wf->period_length_ns;
		}

		if (!pwm_sysfs->cache_valid ||
		    pwm_sysfs->wf.duty_length_ns != wf->duty_length_ns) {
			ret = pwm_chip_sysfs_write_prop(pwm_sysfs, "duty_cycle",
							"%" PRIu64 "\n", wf->duty_length_ns);
			if (ret)
				return ret;
			pwm_sysfs->wf.duty_length_ns = wf->duty_length_ns;
		}
	} else {
		if (!!pwm_sysfs->cache_valid ||
		    pwm_sysfs->wf.duty_length_ns != wf->duty_length_ns) {
			ret = pwm_chip_sysfs_write_prop(pwm_sysfs, "duty_cycle",
							"%" PRIu64 "\n", wf->duty_length_ns);
			if (ret)
				return ret;
			pwm_sysfs->wf.duty_length_ns = wf->duty_length_ns;
		}

		/*
		 * It's already known that
		 * pwm_sysfs->wf.period_length_ns > wf->period_length_ns, so
		 * no need to check for pwm_sysfs->wf.period_length_ns != wf->perio_lengthd
		 */
		ret = pwm_chip_sysfs_write_prop(pwm_sysfs, "period",
						"%" PRIu64 "\n", wf->period_length_ns);
		if (ret)
			return ret;
		pwm_sysfs->wf.period_length_ns = wf->period_length_ns;
	}

	if (!pwm_sysfs->cache_valid || !pwm_sysfs->enabled) {
		ret = pwm_chip_sysfs_write_prop(pwm_sysfs, "enable", "1\n");
		if (ret)
			return ret;
		pwm_sysfs->enabled = true;
	}
	pwm_sysfs->cache_valid = true;

	return 0;
}

static int pwm_chip_sysfs_get_waveform(struct pwm *pwm, struct pwm_waveform *wf)
{
	(void)pwm; (void)wf;
	errno = EIO;
	return -1;
}

struct pwm_chip *pwm_chip_sysfs_open_by_number(unsigned int num)
{
	struct pwm_chip_sysfs *chip_sysfs;
	struct pwm_chip *chip;
	long npwm;
	int dirfd, fd;
	ssize_t ret;
	char buf[128];

	ret = snprintf(buf, sizeof(buf), "/sys/class/pwm/pwmchip%d", num);
	if (ret < 0)
		return NULL;
	if ((size_t)ret >= sizeof(buf)) {
		/* huh */
		errno = EINVAL;
		return NULL;
	}

	dirfd = open(buf, O_PATH | O_CLOEXEC);
	if (dirfd < 0)
		return NULL;

	fd = openat(dirfd, "npwm", O_RDONLY | O_CLOEXEC);
	if (fd < 0) {
		close(dirfd);
		return NULL;
	}

	ret = read(fd, buf, sizeof(buf));
	close(fd);
	if (ret < 0) {
		close(dirfd);
		return NULL;
	}

	if (ret == 0 || buf[ret - 1] != '\n') {
		close(dirfd);
		errno = EINVAL;
		return NULL;
	}

	errno = 0;
	npwm = strtol(buf, NULL, 10);

	if (errno) {
		close(dirfd);
		return NULL;
	}

	if (npwm < 0 || npwm > 128) {
		close(dirfd);
		errno = EINVAL;
		return NULL;
	}

	chip_sysfs = calloc(1, sizeof(*chip_sysfs) + npwm * sizeof(chip_sysfs->pwms[0]));
	if (!chip_sysfs) {
		close(dirfd);
		return NULL;
	}

	chip = &chip_sysfs->chip;
	chip->close = pwm_chip_sysfs_close;
	chip->get_pwm = pwm_chip_sysfs_get_pwm;
	chip->set_waveform = pwm_chip_sysfs_set_waveform;
	chip->get_waveform = pwm_chip_sysfs_get_waveform;
	chip->npwm = npwm;

	chip_sysfs->dirfd = dirfd;

	return chip;
}
