/* SPDX-License-Identifier: LGPL-2.1 */
/* SPDX-FileCopyrightText: 2023 Uwe Kleine-König <u.kleine-koenig@pengutronix.de> */

#include "config.h"

#include <errno.h>
#include <stdlib.h>
#include <stdint.h>

#include <pwm.h>

#include "pwm-internal.h"

struct pwm_chip *pwm_chip_open_by_number(unsigned int num)
{
	struct pwm_chip *chip;

#if HAVE_PWMCDEV
	chip = pwm_chip_cdev_open_by_number(num);
	if (chip == NULL && errno == ENOENT)
		chip = pwm_chip_sysfs_open_by_number(num);
#else
	chip = pwm_chip_sysfs_open_by_number(num);
#endif

	return chip;
}

void pwm_chip_close(struct pwm_chip *chip)
{
	chip->close(chip);
}

int pwm_chip_num_pwms(struct pwm_chip *chip)
{
	return chip->npwm;
}

struct pwm *pwm_chip_get_pwm(struct pwm_chip *chip, unsigned int offset)
{
	if (offset >= chip->npwm) {
		errno = ENOENT;
		return NULL;
	}
	return chip->get_pwm(chip, offset);
}

int pwm_set_waveform(struct pwm *pwm, const struct pwm_waveform *wf)
{
	if (wf->period_length_ns > INT64_MAX ||
	    wf->duty_length_ns > wf->period_length_ns ||
	    (wf->duty_offset_ns && wf->duty_offset_ns >= wf->period_length_ns)) {
		errno = EINVAL;
		return -1;
	}

	return pwm->chip->set_waveform(pwm, wf);
}

int pwm_disable(struct pwm *pwm)
{
	const struct pwm_waveform wf = { .period_length_ns = 0, };

	return pwm->chip->set_waveform(pwm, &wf);
}

int pwm_get_waveform(struct pwm *pwm, struct pwm_waveform *wf)
{
	return pwm->chip->get_waveform(pwm, wf);
}

/* These are old names, but they are binary compatible */
extern int pwm_apply_state(struct pwm *pwm, const struct pwm_waveform *wf)
	__attribute ((alias ("pwm_set_waveform")));
extern int pwm_get_state(struct pwm *pwm, struct pwm_waveform *wf)
	__attribute ((alias ("pwm_get_waveform")));
