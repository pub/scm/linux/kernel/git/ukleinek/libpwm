/* SPDX-License-Identifier: 0BSD */
/* SPDX-FileCopyrightText: 2023 Uwe Kleine-König <u.kleine-koenig@pengutronix.de> */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include <pwm.h>

int main(int argc, char *const argv[])
{
	struct pwm_chip *chip;
	struct pwm *pwm;
	struct pwm_waveform wf = {
		.period_length_ns = 50000,
		.duty_length_ns = 25000,
		.duty_offset_ns = 0,
	};
	long long sleepns = 5000000000;
	int verbose = 0;
	int npwm;
	int ret;
	int opt;

	unsigned int chipno = 0;
	unsigned int pwmno = 0;

	while ((opt = getopt(argc, argv, "c:p:s:P:D:O:v")) != -1) {
		switch (opt) {
		case 'c':
			chipno = atoi(optarg);
			break;
		case 'p':
			pwmno = atoi(optarg);
			break;
		case 's':
			sleepns = atoll(optarg);
			break;
		case 'P':
			wf.period_length_ns = atoll(optarg);
			break;
		case 'D':
			wf.duty_length_ns = atoll(optarg);
			break;
		case 'O':
			wf.duty_offset_ns = atoll(optarg);
			break;
		case 'v':
			verbose = 1;
			break;

		default:
			return EXIT_FAILURE;
		}
	}

	chip = pwm_chip_open_by_number(chipno);
	if (!chip) {
		perror("Failed to open pwmchip0");
		return EXIT_FAILURE;
	}

	if (verbose) {
		npwm = pwm_chip_num_pwms(chip);
		if (npwm < 0) {
			perror("Failed to determine count of pwm");
			return EXIT_FAILURE;
		}

		printf("Chip supports %u PWMs\n", npwm);
	}

	pwm = pwm_chip_get_pwm(chip, pwmno);
	if (!pwm) {
		perror("Failed to get pwm0");
		return EXIT_FAILURE;
	}

	ret = pwm_set_waveform(pwm, &wf);
	if (ret < 0) {
		perror("Failed to configure PWM");
		return EXIT_FAILURE;
	}

	nanosleep(&(struct timespec){ .tv_sec = sleepns / 1000000000, .tv_nsec = sleepns % 1000000000 }, NULL);

	pwm_chip_close(chip);

	return EXIT_SUCCESS;
}
