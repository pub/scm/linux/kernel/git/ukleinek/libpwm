/* SPDX-License-Identifier: LGPL-2.1-only */
/* SPDX-FileCopyrightText: 2023 Uwe Kleine-König <u.kleine-koenig@pengutronix.de> */

#include <pwm.h>

#define container_of(ptr, type, member) ({				\
	(type *)((void *)(ptr) - __builtin_offsetof(type, member)); })

struct pwm_chip {
	unsigned int npwm;
	void (*close)(struct pwm_chip *chip);
	struct pwm *(*get_pwm)(struct pwm_chip *chip, unsigned int offset);
	int (*set_waveform)(struct pwm *pwm, const struct pwm_waveform *wf);
	int (*get_waveform)(struct pwm *pwm, struct pwm_waveform *wf);
};

struct pwm {
	struct pwm_chip *chip;
};

struct pwm_chip *pwm_chip_cdev_open_by_number(unsigned int num);
struct pwm_chip *pwm_chip_sysfs_open_by_number(unsigned int num);
