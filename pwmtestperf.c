/* SPDX-License-Identifier: 0BSD */
/* SPDX-FileCopyrightText: 2023 Uwe Kleine-König <u.kleine-koenig@pengutronix.de> */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <pwm.h>

int main(int argc, char *const argv[])
{
	struct pwm_chip *chip;
	struct pwm *pwm;
	struct pwm_waveform wf = {
		.period_length_ns = 50000,
		.duty_length_ns = 0,
		.duty_offset_ns = 0,
	};
	int ret;
	int opt;
	long long step = 1;

	unsigned int chipno = 0;
	unsigned int pwmno = 0;

	while ((opt = getopt(argc, argv, "c:p:P:S:")) != -1) {
		switch (opt) {
		case 'c':
			chipno = atoi(optarg);
			break;

		case 'p':
			pwmno = atoi(optarg);
			break;

		case 'P':
			wf.period_length_ns = atoll(optarg);
			break;

		case 'S':
			step = atoll(optarg);
			if (!step) {
				errno = EINVAL;
				perror("Invalid step value");
				return EXIT_FAILURE;
			}
			break;

		default:
			return EXIT_FAILURE;
		}
	}

	chip = pwm_chip_open_by_number(chipno);
	if (!chip) {
		perror("Failed to open pwmchip0");
		return EXIT_FAILURE;
	}

	pwm = pwm_chip_get_pwm(chip, pwmno);
	if (!pwm) {
		perror("Failed to get pwm0");
		return EXIT_FAILURE;
	}

	for (wf.duty_length_ns = (step > 0 ? 0 : wf.period_length_ns); wf.duty_length_ns <= wf.period_length_ns; wf.duty_length_ns += step) {
		ret = pwm_set_waveform(pwm, &wf);
		if (ret < 0) {
			perror("Failed to configure PWM");
			return EXIT_FAILURE;
		}
	}

	pwm_chip_close(chip);

	return EXIT_SUCCESS;
}
