/* SPDX-License-Identifier: LGPL-2.1-only */
/* SPDX-FileCopyrightText: 2023 Uwe Kleine-König <u.kleine-koenig@pengutronix.de> */

#ifndef __LIBPWM_PWM_H__
#define __LIBPWM_PWM_H__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct pwm_chip;
struct pwm;

struct pwm_waveform {
	uint64_t period_length_ns;
	uint64_t duty_length_ns;
	uint64_t duty_offset_ns;
};

struct pwm_chip *pwm_chip_open_by_number(unsigned int num);
void pwm_chip_close(struct pwm_chip *chip);

int pwm_chip_num_pwms(struct pwm_chip *chip);

struct pwm *pwm_chip_get_pwm(struct pwm_chip *chip, unsigned int offset);

int pwm_set_waveform(struct pwm *pwm, const struct pwm_waveform *wf);

int pwm_get_waveform(struct pwm *pwm, struct pwm_waveform *wf);

#ifdef __cplusplus
}
#endif

#endif /* ifndef __LIBPWM_PWM_H__ */
